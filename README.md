# NativeBase KitchenSink v2.5.0
An example app with all the UI components of NativeBase

## Demo

https://expo.io/@geekyants/nativebasekitchensink

iOS | Android
 :--:| :-----:
 ![ios-demo](https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/master/screenshots/iOS.gif) | ![android-demo](https://github.com/GeekyAnts/NativeBase-KitchenSink/raw/master/screenshots/Android.gif)

This project was bootstrapped with [Create React Native App](https://github.com/react-community/create-react-native-app).

Below you'll find information about performing common tasks. The most recent version of this guide is available [here](https://github.com/react-community/create-react-native-app/blob/master/react-native-scripts/template/README.md).

## Installation

*	**Clone and install packages**
```
git clone git@gitlab.com:u2344/fresh-native-base.git
cd fresh-native-base
yarn or npm
```

*	**Run on iOS**
	*	Opt #1:
		*	Run `npm start` in your terminal
		*	Scan the QR code in your Expo app
	*	Opt #2:
		*	Run `npm run ios` in your terminal


*	**Run on Android**
	*	Opt #1:
		*	Run `npm start` in your terminal
		*	Scan the QR code in your Expo app
	*	Opt #2:
		*	Make sure you have an `Android emulator` installed and running
		*	Run `npm run android` in your terminal
